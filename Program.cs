﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace persistenta
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduceti un numar!");
            if(! int.TryParse(Console.ReadLine(),out int nr))
            {
                Console.WriteLine("Numarul introdus nu este valid!");
            }
            int persistenta = Persistenta(nr);
            Console.WriteLine("Persistenta numarului "+nr+" este: "+persistenta);
            Console.ReadKey();
        }
        static int Persistenta(int n)
        {
            int p = 1;
            int k = 0;
            bool gata = false;
            while (gata == false)
            { 
                p = p * (n % 10);
                Console.WriteLine(p+" ");
                n = n / 10;
                if(n==0)
                {
                    k++;
                    n = p;
                    p = 1;
                if(n/10==0)
                {
                    gata = true;
                }
                }
            }
            return k;
        }

    }
}
